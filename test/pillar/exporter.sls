rabbitmq_exporter:
  bin_dir: '/usr/bin'
  dist_dir: '/opt/rabbitmq_exporter/dist'
  version: '0.29.0'
  service: True
  service_user: 'rabbitmq_exporter'
  service_group: 'rabbitmq_exporter'
  ssl: False
  cafile: /srv/certs/bundleca.pem
  rabbitmq_url: 'http://localhost:15672'
  rabbitmq_user: admin
  rabbitmq_password: password

#  options:
#    - 'version'
