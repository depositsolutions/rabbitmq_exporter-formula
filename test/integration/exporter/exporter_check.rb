describe http('http://127.0.0.1:9419/metrics') do
  its('status') { should eq 200 }
  its('body') { should match /rabbitmq_up 1/ }
  its('body') { should match /rabbitmq_running{node="rabbit@rabbitmq_exporter-formula"} 1/ }
end
