{% from slspath+"/map.jinja" import rabbitmq_exporter with context %}

rabbitmq_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/rabbitmq_exporter.service
    - source:  salt://{{ slspath }}/files/exporter.service.j2
    - template: jinja
    - context:
        rabbitmq_exporter: {{ rabbitmq_exporter }}

rabbitmq_exporter-service:
  service.running:
    - name: rabbitmq_exporter
    - enable: True
    - start: True
    - watch:
      - file: rabbitmq_exporter-install-service
      - file: rabbitmq_exporter-create-env

rabbitmq_exporter-create-env:
  file.managed:
    - name: /etc/systemd/system/rabbitmq_exporter.service.d/env.conf
    - source: salt://{{ slspath }}/files/env.conf.j2
    - makedirs: True
    - template: jinja
    - context:
        rabbitmq_exporter: {{ rabbitmq_exporter }}
    - user: root
    - group: root
    - mode: 644
